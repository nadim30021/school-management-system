<%@ page import="java.util.ArrayList"%>

<!DOCTYPE html>
<style>
#all{
width:60%;
padding:.4%;
margin-right: auto;
margin-left: auto;
border: thin solid black;

}
</style>

<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<html>
<head>
	<title>Teacher DB</title>
</head>
<body>
<div class="jumbotron text-center">
<center><h3>School Management System</h3></center>
	


	<table border="1" width="100%">
		<tr>
			<td width="16%" style="text-align: center;"><a href="adminHome"/>Home</td>
			<td width="16%" style="text-align: center;"><a href="adminstudentdb"/>Student DB</td>
			<td width="16%" style="text-align: center;"><a href="adminteacherdb"/>Teacher DB</td>
			<td width="16%" style="text-align: center;"><a href="adminInbox"/>Inbox</td>
			<td width="16%" style="text-align: center;"><a href="#"/>Reports</td>
			<td width="16%" style="text-align: center;"><a href="logoutt"/>Logout</td>
		</tr>
	</table>
	
</div>
<fieldset>
<center>
<P> Teacher DB</P>
 <div id="all">Teacher List</div><br/><br/>
  
<fieldset>
	
<%
	ArrayList<String> sid = (ArrayList<String>)request.getAttribute("teacher_id");
	ArrayList<String> sname = (ArrayList<String>)request.getAttribute("teacher_name");
	ArrayList<String> email = (ArrayList<String>)request.getAttribute("email");
	ArrayList<String> pass = (ArrayList<String>)request.getAttribute("password");
	ArrayList<String> add = (ArrayList<String>)request.getAttribute("address");
	ArrayList<String> dob = (ArrayList<String>)request.getAttribute("DOB");
	ArrayList<String> fname = (ArrayList<String>)request.getAttribute("fathers_name");
	ArrayList<String> mname = (ArrayList<String>)request.getAttribute("mothers_name");

%>

<div style="width: 95%">

	<span style="width: 10.75% ; display:inline-block;">
		Student ID<br/><br/><hr>
		<%for(String stid:sid){%>
			<%=stid%><br/><hr>
	<%}%>
	</span>

	<span style="width: 10.75% ; display:inline-block;">
		Student Name<br/><br/><hr>
		<%for(String stname:sname){%>
			<%=stname%><br/><hr>
		<%}%>
	</span>

	<span style="width: 10.75% ; display:inline-block;">
		Class<br/><br/><hr>
		<%for(String em:email ){%>
		<%=em%><br/><hr>
		<%}%>
	</span>

	<span style="width: 10.75% ; display:inline-block;">
		Password<br/><br/><hr>
		<%for(String passw:pass){%>
			<%=passw%><br/><hr>
			<%}%>
	</span>

	<span style="width: 10.75% ; display:inline-block;">
		Address<br/><br/><hr>
		<%for(String addr:add){%>
			<%=addr%><br/><hr>
		<%}%>
	</span>

	<span style="width: 10.75% ; display:inline-block;">
		Date Of Birth<br/><br/><hr>
		<%for(String dtob:dob){%>
		<%=dtob%><br/><hr>
		<%}%>
	</span>


	<span style="width: 10.75% ; display:inline-block;">
		Father's Name<br/><br/><hr>
		<%for(String faname:fname){%>
		<%=faname%><br/><hr>
		<%}%>
	</span>

	<span style="width: 10.75% ; display:inline-block;">
		Mothers's Name<br/><br/><hr>
		<%for(String moname:mname){%>
		<%=moname%><br/><hr>
		<%}%>
	</span>

	<span style="width: 10.75% ; display:inline-block;">
		Manege<br/><br/><hr>
		<%for(String stid:sid){%>
		<a href="#">Details</a><br/><hr>
		<%}%>
	</span>
<div>
</fieldset>
</fieldset>
</center>
</body>
</html>