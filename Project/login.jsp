<!DOCTYPE html>
<style>
#all{
width:97%;
padding:.4%;
margin-right: auto;
margin-left: auto;
border: thin solid black;
}

#rspan{
width:20%;
padding:.10%;

border: thin solid black;
}

p {
  position: absolute;
  top: auto;
}

</style>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <link rel="stylesheet" type="text/css" href="loginstyle.css" />

<html>
<head>
	<title>Login</title>
</head>
<body>


<div class="jumbotron text-center">
  <center>
  <h1>School Management System</h1>
  <br/>

  <table border="1" width="100%">
		<tr>
			<td width="20%" style="text-align: center;"><a href="information.jsp"/>Information</td>
			<td width="20%" style="text-align: center;"><a href="notice"/>Notice</td>
			<td width="20%" style="text-align: center;"><a href="contact.jsp"/>Contact</td>
			<td width="20%" style="text-align: center;"><a href="login.jsp"/>Login</td>
		</tr>
	</table>
  </center>
</div>


<fieldset>
<center>
	<h3>Login</h3>

<div class="container">
        <div class="card card-container">
            <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
            <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
            <p id="profile-name" class="profile-name-card"></p>
            <form class="form-signin" method="post">
                <span id="reauth-email" class="reauth-email"></span>
                <input type="text" id="inputEmail" name="username" class="form-control" placeholder="User Name" required autofocus>
                <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>

                <select id='type' name='type' class="form-control" required autofocus >
					<option value='student'>Student</option>
					<option value='teacher'>Teacher</option>
					<option value='admin'>Admin</option>
				</select>

                <div id="remember" class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> Remember me
                    </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
            </form><!-- /form -->
            <a href="#" class="forgot-password">Forgot the password?</a>
        </div><!-- /card-container -->
    </div><!-- /container -->
</fieldset>
</body>
</html>