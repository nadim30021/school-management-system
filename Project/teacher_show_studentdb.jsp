<%@ page import="java.util.ArrayList"%>

<!DOCTYPE html>
<style>
#all{
width:60%;
padding:.4%;
margin-right: auto;
margin-left: auto;
border: thin solid black;

}
</style>

<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<html>
<head>
	<title>Student DB</title>
</head>
<body>
<div class="jumbotron text-center">
<center><h3>School Management System</h3></center>
	


	<table border="1" width="100%">
		<tr>
			<td width="20%" style="text-align: center;"><a href="teacherstudentdb"/>Student DB</td>
			<td width="20%" style="text-align: center;"><a href="teacheraddmarks"/>Add Marks</td>
			<td width="20%" style="text-align: center;"><a href="teacherInbox"/>Inbox</td>
			<td width="20%" style="text-align: center;"><a href="#"/>Report</td>
			<td width="20%" style="text-align: center;"><a href="logoutt"/>Logout</td>
		</tr>
	</table>
	
</div>
<fieldset>
<center>
<P> Student DB</P>
 <div id="all">Student By Class</div>
  <form method="post" action="adminstudentdb">
<div id="all"><select name="classs">
  <option value="1">1</option>
  <option value="2">2</option>
  <option value="3">3</option>
  <option value="4">4</option>
  <option value="5">5</option>
  <option value="6">6</option>
  <option value="7">7</option>
  <option value="8">8</option>
  <option value="9">9</option>
  <option value="10">10</option>
</select> &nbsp; &nbsp; <input type="submit" name="buttonStudent" value="Search"></div><br/><br/>
</form>


 </center>
<fieldset>
	
<%
	ArrayList<String> sid = (ArrayList<String>)request.getAttribute("student_id");
	ArrayList<String> sname = (ArrayList<String>)request.getAttribute("student_name");
	ArrayList<String> cls = (ArrayList<String>)request.getAttribute("cl");
	ArrayList<String> sub = (ArrayList<String>)request.getAttribute("subject");
	ArrayList<String> marks = (ArrayList<String>)request.getAttribute("marks");
	

%>

<div style="width: 95%">

	<span style="width: 16% ; display:inline-block;">
		Student ID<br/><br/><hr>
		<%for(String stid:sid){%>
			<%=stid%><br/><hr>
	<%}%>
	</span>

	<span style="width: 16% ; display:inline-block;">
		Student Name<br/><br/><hr>
		<%for(String stname:sname){%>
			<%=stname%><br/><hr>
		<%}%>
	</span>

	<span style="width: 16% ; display:inline-block;">
		Class<br/><br/><hr>
		<%for(String clss:cls){%>
		<%=clss%><br/><hr>
		<%}%>
	</span>

	<span style="width: 16% ; display:inline-block;">
		Subject <br/><br/><hr>
		<%for(String subject:sub){%>
			<%=subject%><br/><hr>
			<%}%>
	</span>

	<span style="width: 16% ; display:inline-block;">
		Marks<br/><br/><hr>
		<%for(String mark:marks){%>
			<%=mark%><br/><hr>
		<%}%>
	</span>

	<span style="width: 16% ; display:inline-block;">
		Manage<br/><br/><hr>
		<%for(String stid:sid){%>
		<a href="#">Details</a><br/><hr>
		<%}%>
	</span>
<div>
</fieldset>
</fieldset>
</body>
</html>