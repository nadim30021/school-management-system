<!DOCTYPE html>
<style>
#all{
width:60%;
padding:.4%;
margin-right: auto;
margin-left: auto;
border: thin solid black;

}
</style>


<html>
<head>
	<title>Admin home add</title>
	

	<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	  <link rel="stylesheet" type="text/css" href="registrationstyle.css" />
</head>
<body>
<div class="jumbotron text-center">
<center>
  <h1>School Management System</h1>
  <br/>
	<table border="1" width="100%">
		<tr>
			<td width="16%" style="text-align: center;"><a href="adminHome"/>Home</td>
			<td width="16%" style="text-align: center;"><a href="adminstudentdb"/>Student DB</td>
			<td width="16%" style="text-align: center;"><a href="adminteacherdb"/>Teacher DB</td>
			<td width="16%" style="text-align: center;"><a href="adminInbox"/>Inbox</td>
			<td width="16%" style="text-align: center;"><a href="#"/>Reports</td>
			<td width="16%" style="text-align: center;"><a href="logoutt"/>Logout</td>
		</tr>
	</table>
	</div>
	
<fieldset>
<center><h3>ADMIN HOME</h3>
<h5> ADD</h5>


 <div class="container">
      <div class="row main">
        <div class="main-login main-center">
        <center><h4>Sign up</h4></center>

          <form class="" method="post" action="adminHome">
            
            <div class="form-group">
              <label for="name" class="cols-sm-2 control-label">Id : </label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                  <input type="text" required="required" class="form-control" name="id" id="id"  placeholder="Enter Student ID"/>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="email" class="cols-sm-2 control-label">Name : </label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                  <input type="text" required="required" class="form-control" name="name" id="name"  placeholder="Enter your Email"/>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="phone" class="cols-sm-2 control-label">Fathers Name</label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-mobile fa" aria-hidden="true"></i></span>
                  <input type="text" required="required" class="form-control" name="fname" id="fname"  placeholder="Fathers Name"/>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="password" class="cols-sm-2 control-label">Mothers Name</label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                  <input type="text"  required="required" class="form-control" name="mname" id="mname" onchange="form.confirm.pattern = this.value;" placeholder="Mothers Name"/>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="confirm" class="cols-sm-2 control-label">DOB</label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                  <input type="text" required="required" class="form-control" name="dob" id="dob"  placeholder="DD/MM/YYYY"/>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="password" class="cols-sm-2 control-label">Address</label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                  <textarea rows="4" cols="30" name="address" required="required" class="form-control"  onchange="form.confirm.pattern = this.value;" placeholder="Address"></textarea>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="confirm" class="cols-sm-2 control-label">Password</label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                  <input type="text" required="required" class="form-control" name="password" id="password"  placeholder="Password"/>
                </div>
              </div>
            </div>


            <div class="form-group">
              <label for="confirm" class="cols-sm-2 control-label">Type : </label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                  <select name="type" id="type" style=" border: thin solid black; width: 60%; text-align: center;"  onchange="slcttype()">
                  <option value="teacher">Teacher</option>
                  <option value="student">Student</option>
                </select>
                </div>
              </div>
            </div>

           <span name="classname" id="classname">Email :</span>

           <span name="classs" id="classs"><input type='email' name='email' /></span>




              <div class="form-group ">
              <input type="submit" name="Submit" value="Submit" class="btn btn-primary btn-lg btn-block login-button"/>
            </div>
            
          </form>
        </div>
      </div>
    </div>





 </center>

</br> </br> </br> </br> </br> </br></br> </br> </br></br> </br>
</fieldset>
</body>
</html>



<script>


function slcttype()
{
	
	var type=document.getElementById("type").value;
	
	if(type=="student")
	{
		var v= "<select name='class' style=' border: thin solid black; text-align: center;'>  <option value=''>Class</option>  <option value='1'>1</option>  <option value='2'>2</option>  <option value='3'>3</option>  <option value='4'>4</option></select>";
		 document.getElementById("classname").innerHTML ="Class :";
		 document.getElementById("classs").innerHTML =v;
	}
	else
	{
		document.getElementById("classname").innerHTML ="Email :";
		document.getElementById("classs").innerHTML="<input type='email' name='email' />";
	}

}

</script>
