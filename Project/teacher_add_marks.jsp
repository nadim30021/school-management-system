<%@ page import="java.util.ArrayList"%>
<!DOCTYPE html>
<style>
#all{
width:60%;
padding:.4%;
margin-right: auto;
margin-left: auto;
border: thin solid black;

}
#right{
float: right;
}
</style>

<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<html>
<head>
	<title>Teacher Add Marks</title>
</head>
<body>
<div class="jumbotron text-center">
<center><h3>School Management System</h3></center>

	<table border="1" width="100%">
		<tr>
			<td width="20%" style="text-align: center;"><a href="teacherstudentdb"/>Student DB</td>
			<td width="20%" style="text-align: center;"><a href="teacheraddmarks"/>Add Marks</td>
			<td width="20%" style="text-align: center;"><a href="teacherInbox"/>Inbox</td>
			<td width="20%" style="text-align: center;"><a href="#"/>Report</td>
			<td width="20%" style="text-align: center;"><a href="logoutt"/>Logout</td>
		</tr>
	</table>
</div>

<fieldset>
<center>
<p>Add Marks</p>
<hr>
<form> 
<table cellspacing="4" cellpadding="4">
	<tr>
		<td>Class : </td>
		<td>
			<select name="class" style=" border: thin solid black; width: 100%; text-align: center;">
			  <option value="1">1</option>
			  <option value="2">2</option>
			  <option value="3">3</option>
			  <option value="4">4</option>
			  <option value="5">5</option>
			  <option value="6">6</option>
			  <option value="7">7</option>
			  <option value="8">8</option>
			  <option value="9">9</option>
			  <option value="10">10</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>Subject</td>
		<td>
			<select name="subject" style=" border: thin solid black; width: 100%; text-align: center;">
			  <option value="Bangla">Bangla</option>
			  <option value="English">English</option>
			  <option value="Math">MathMatics</option>
			  <option value="Physics">Physics</option>
			  <option value="Chemistry">Chemistry</option>
			  <option value="Biology">Biology</option>
			  <option value="Religion">Religion</option>
			  <option value="Social Science">Social Science</option>
			</select>
		</td>
	</tr>
	<tr>
		<td></td>
		<td><input type="submit" name="Submit"  value="Get List" ></td>
	</tr>
</table>
</form>

<br/><br/><br/>
<%
ArrayList<String> sid = (ArrayList<String>)request.getAttribute("sid");
ArrayList<String> sname = (ArrayList<String>)request.getAttribute("sname");
%>
<fieldset>
<form method="post" action="markInsert">
	<span style="width: 23% ; display:inline-block;">
		Student ID<br/><br/><hr>
		<%for(String stid:sid){%>
			<%=stid%><br/><hr>
	<%}%>
	</span>

	<span style="width: 23% ; display:inline-block;">
		Student Name<br/><br/><hr>
		<%for(String stname:sname){%>
			<%=stname%><br/><hr>
		<%}%>
	</span>

	<span style="width: 23% ; display:inline-block;">
		Marks <br/><br/><hr>
		<%for(String stid:sid){%>
		<input type="text" name="<%=stid%>"><br/><hr>
		<%}%>
	</span>

	<span style="width: 23% ; display:inline-block;">
		ReMarks <br/><br/><hr>
		<%for(String stid:sid){%>
		<input type="text" name="<%=stid%>r"><br/><hr>
		<%}%>
	</span>

<br/><br/>
<center><input type="submit" name="buttonsubmit" value="Add Marks"></center>
</form>

</fieldset>



</center>
</br> </br>
</fieldset>
</body>
</html>
