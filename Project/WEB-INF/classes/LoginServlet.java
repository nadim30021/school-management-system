

import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;

import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;
import java.sql.SQLException;

public class LoginServlet extends HttpServlet{
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  try {

				    RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
					dispatcher.forward(request, response); 
		  			
			}catch(Exception ex){
				
			}
	}	





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  try {

		  		
		  		String id = request.getParameter("username");
		  		String pass = request.getParameter("password");
		  		String type= request.getParameter("type");

		  		
				MySqlDataAccess dataAccess = new MySqlDataAccess();
				

		  		if(type.equals("student"))
		  		{		  		
				String query = "SELECT id FROM student  WHERE id='" + id+"' and password = '"+pass+"'";				
				ResultSet rs = dataAccess.getData(query);

					if(rs.next()){

						HttpSession session=request.getSession();  
        				session.setAttribute("userid",id);
        				session.setAttribute("usertype","student");

						String gradequery = "SELECT * FROM grade WHERE student_id="+id;
						ResultSet rs1 = dataAccess.getData(gradequery);

						
						while(rs1.next()){
							request.setAttribute(rs1.getString("Subject_Name"), rs1.getString("Marks"));
						}	

						
						RequestDispatcher dispatcher = request.getRequestDispatcher("student_home.jsp");
						dispatcher.forward(request, response);
						

						   
					}
					else
					{
						     RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
							dispatcher.forward(request, response); 
					}

				}


				else if(type.equals("admin"))
		  		{		  		
				String query = "SELECT id FROM admin  WHERE id='" + id+"' and password = '"+pass+"'";				
				ResultSet rs = dataAccess.getData(query);

					if(rs.next()){

						HttpSession session=request.getSession();  
        				session.setAttribute("userid",id);
        				session.setAttribute("usertype","admin");

												
						RequestDispatcher dispatcher = request.getRequestDispatcher("admin_home_add.jsp");
						dispatcher.forward(request, response);
						

						   
					}
					else
					{
						     RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
							dispatcher.forward(request, response); 
					}

				}

				else if(type.equals("teacher"))
		  		{		  		
				String query = "SELECT id FROM teacher  WHERE id='" + id+"' and password = '"+pass+"'";				
				ResultSet rs = dataAccess.getData(query);

					if(rs.next()){

						HttpSession session=request.getSession();  
        				session.setAttribute("userid",id);
        				session.setAttribute("usertype","teacher");

												
						RequestDispatcher dispatcher = request.getRequestDispatcher("teacher_home.jsp");
						dispatcher.forward(request, response);
						

						   
					}
					else
					{
						     RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
							dispatcher.forward(request, response); 
					}

				}



	
		  			
			}catch(Exception ex){
				
			}
	}	
}