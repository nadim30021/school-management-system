import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;

import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;
import java.sql.SQLException;

public class AdminInboxServlet extends HttpServlet{
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  try {
		  		HttpSession session=request.getSession(false);  
        		String n=(String)session.getAttribute("userid");
        		String p=(String)session.getAttribute("usertype");

        		if(p.equals("admin")){

        			ArrayList<String> email_details = new ArrayList<String>();
					

        			MySqlDataAccess dataAccess = new MySqlDataAccess();


        			String query = "SELECT * FROM admin_email";				
					ResultSet rs = dataAccess.getData(query);

					while(rs.next()){
						
							email_details.add(rs.getString("sender_email")+" : "+rs.getString("body"));
							
							
					}

					request.setAttribute("email",email_details);
					request.setAttribute("id",n);
													
					RequestDispatcher dispatcher = request.getRequestDispatcher("admin_inbox.jsp");
					dispatcher.forward(request, response);

				}
				else
				{
					RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
					dispatcher.forward(request, response);
				}
				
	
		  			
			}catch(Exception ex){}
	}	

	
	

}