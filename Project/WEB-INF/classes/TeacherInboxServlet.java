import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;

import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;
import java.sql.SQLException;

public class TeacherInboxServlet extends HttpServlet{
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  try {
		  		HttpSession session=request.getSession(false);  
        		String n=(String)session.getAttribute("userid");
        		String p=(String)session.getAttribute("usertype");

        		if(p.equals("teacher")){

        			ArrayList<String> email_details = new ArrayList<String>();
					

        			MySqlDataAccess dataAccess = new MySqlDataAccess();

        			String query1 = "SELECT * FROM teacher where id="+n;
        			ResultSet rs1 = dataAccess.getData(query1);
        			String em = "";

        			while(rs1.next()){
						
							em=rs1.getString("email");
							System.out.println(em);
	
					}


        			String query = "SELECT * FROM teacher_email where sender_email='"+em+"'";				
					ResultSet rs = dataAccess.getData(query);

					while(rs.next()){
						
							email_details.add(rs.getString("sender_email")+" : "+rs.getString("student_id")+" : "+rs.getString("email_subject")+" : "+rs.getString("email_body"));
						
							
					}

					request.setAttribute("email",email_details);
					request.setAttribute("id",n);
													
					RequestDispatcher dispatcher = request.getRequestDispatcher("teacher_inbox.jsp");
					dispatcher.forward(request, response);

				}
				else
				{
					RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
					dispatcher.forward(request, response);
				}
				
	
		  			
			}catch(Exception ex){}
	}	


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  try {
		  		HttpSession session=request.getSession(false);  
        		String n=(String)session.getAttribute("userid");
        		String p=(String)session.getAttribute("usertype");

        		if(p.equals("teacher")){

        			String email = request.getParameter("email");

        			MySqlDataAccess dataAccess = new MySqlDataAccess();
        			String query = "SELECT * FROM teacher where id='"+n+"'";				
					ResultSet rs = dataAccess.getData(query);
					String temail="";
					System.out.println("1st");
					while(rs.next()){
					temail = rs.getString("email");
					}
					
        			String query1 = "INSERT into admin_email(sender_email,body) values('"+temail+"','"+email+"')";
					dataAccess.executeQuery(query1);
					
					request.setAttribute("id", n);

					    response.sendRedirect("teacherInbox");  
							
					//RequestDispatcher dispatcher = request.getRequestDispatcher("teacher_inbox.jsp");
					//dispatcher.forward(request, response);

				}
				else
				{
					RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
					dispatcher.forward(request, response);
				}
				
	
		  			
			}catch(Exception ex){}
	}	

	
	

}