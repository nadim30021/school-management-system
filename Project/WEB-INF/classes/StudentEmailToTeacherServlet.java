

import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;

import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;
import java.sql.SQLException;

public class StudentEmailToTeacherServlet extends HttpServlet{
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  try {



		  		HttpSession session=request.getSession(false);  
        		String n=(String)session.getAttribute("userid");
        		String p=(String)session.getAttribute("usertype");

        		if(p.equals("student")){
					request.setAttribute("id",n);
													
					RequestDispatcher dispatcher = request.getRequestDispatcher("student_email_to_teacher.jsp");
					dispatcher.forward(request, response);

				}
				else
				{
					RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
					dispatcher.forward(request, response);
				}
				
	
		  			
			}catch(Exception ex){
				
			}
	}	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  try {

		  		HttpSession session=request.getSession(false);  
        		String n=(String)session.getAttribute("userid");
        		String p=(String)session.getAttribute("usertype");

        		if(p.equals("student")){

        		String email = request.getParameter("teacher_email");
		  		String student_id = request.getParameter("stid");
		  		String subject= request.getParameter("subject");
		  		String body= request.getParameter("body");

		  			MySqlDataAccess dataAccess = new MySqlDataAccess();
        			String query = "INSERT into teacher_email (sender_email,student_id,email_subject,email_body) values('"+email+"','"+student_id+"','"+subject+"','"+body+"')";				
					dataAccess.executeQuery(query);
        		
					request.setAttribute("id", student_id);
													
					RequestDispatcher dispatcher = request.getRequestDispatcher("student_email_to_teacher.jsp");
					dispatcher.forward(request, response);
				}
				else
				{
					RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
					dispatcher.forward(request, response);
				}
				RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
					dispatcher.forward(request, response);
	
			}catch(Exception ex){
				
			}
	}


}