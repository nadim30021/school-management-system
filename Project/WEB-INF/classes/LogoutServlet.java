import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;

import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;
import java.sql.SQLException;

public class LogoutServlet extends HttpServlet{
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  try {
		  		HttpSession session=request.getSession(false);  
        		

        			session.invalidate();
					RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
					dispatcher.forward(request, response);

	
		  			
			}catch(Exception ex){}
	}	

}