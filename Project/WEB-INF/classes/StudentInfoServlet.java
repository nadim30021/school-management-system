

import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;

import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;
import java.sql.SQLException;

public class StudentInfoServlet extends HttpServlet{
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  try {



		  		HttpSession session=request.getSession(false);  
        		String n=(String)session.getAttribute("userid");
        		String p=(String)session.getAttribute("usertype");

        		if(p.equals("student")){


        		MySqlDataAccess dataAccess = new MySqlDataAccess();


        		String query = "SELECT * FROM student WHERE id='"+n+"'";				
				ResultSet rs = dataAccess.getData(query);

					if(rs.next()){

							request.setAttribute("name", rs.getString("student_name"));
							request.setAttribute("fathers_name", rs.getString("fathers_name"));
							request.setAttribute("mothers_name", rs.getString("mothers_name"));
							request.setAttribute("address", rs.getString("address"));
							request.setAttribute("dob", rs.getString("DOB"));
							request.setAttribute("class", rs.getInt("class"));
						
						RequestDispatcher dispatcher = request.getRequestDispatcher("student_info.jsp");
						dispatcher.forward(request, response);

	
					}
				}
				else
				{
					RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
					dispatcher.forward(request, response);
				}
	
		  			
			}catch(Exception ex){
				
			}
	}	
}