import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;

import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;
import java.sql.SQLException;

public class AdminTeacherDbServlet extends HttpServlet{
	
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  try {

		  		HttpSession session=request.getSession(false);  
        		String n=(String)session.getAttribute("userid");
        		String p=(String)session.getAttribute("usertype");

        		

        		if(p.equals("admin")){


        			MySqlDataAccess dataAccess = new MySqlDataAccess();


        			String query = "SELECT * FROM teacher";				
					ResultSet rs = dataAccess.getData(query);

					ArrayList<String> teacher_id = new ArrayList<String>();
					ArrayList<String> teacher_name = new ArrayList<String>();
					ArrayList<String> email= new ArrayList<String>();
					ArrayList<String> password = new ArrayList<String>();
					ArrayList<String> address = new ArrayList<String>();
					ArrayList<String> DOB = new ArrayList<String>();
					ArrayList<String> fathers_name = new ArrayList<String>();
					ArrayList<String> mothers_name = new ArrayList<String>();

					while(rs.next()){
							teacher_id.add(rs.getString("id"));
							teacher_name.add(rs.getString("teacher_name"));
							email.add(rs.getString("email"));
							password.add(rs.getString("password"));
							address.add(rs.getString("address"));
							DOB.add(rs.getString("DOB"));
							fathers_name.add(rs.getString("fathers_name"));
							mothers_name.add(rs.getString("mothers_name"));
					}

					request.setAttribute("teacher_id",teacher_id);
					request.setAttribute("teacher_name",teacher_name);
					request.setAttribute("email",email);
					request.setAttribute("password",password);
					request.setAttribute("address",address);
					request.setAttribute("DOB",DOB);
					request.setAttribute("fathers_name",fathers_name);
					request.setAttribute("mothers_name",mothers_name);
					
					RequestDispatcher dispatcher = request.getRequestDispatcher("admin_show_teacherdb.jsp");
					dispatcher.forward(request, response);

        		
				}
				else
				{
					RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
					dispatcher.forward(request, response);
				}
	
			}catch(Exception ex){}
	}

}