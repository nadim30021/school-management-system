import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;

import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;
import java.sql.SQLException;

public class TeacherAddMarksServlet extends HttpServlet{
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  try {
		  		HttpSession session=request.getSession(false);  
        		String n=(String)session.getAttribute("userid");
        		String p=(String)session.getAttribute("usertype");

        		if(p.equals("teacher")){

        			request.setAttribute("id",n);
													
					RequestDispatcher dispatcher = request.getRequestDispatcher("teacher_select_for_add_marks.jsp");
					dispatcher.forward(request, response);

				}
				else
				{
					RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
					dispatcher.forward(request, response);
				}
				
	
		  			
			}catch(Exception ex){}
	}	


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  try {
		  		HttpSession session=request.getSession();  
        		String n=(String)session.getAttribute("userid");
        		String p=(String)session.getAttribute("usertype");

        		if(p.equals("teacher")){

        			String classs = request.getParameter("classs");
        			session.setAttribute("cls",classs);
        			session.setAttribute("subject",request.getParameter("subject"));


        			MySqlDataAccess dataAccess = new MySqlDataAccess();
        			String query = "SELECT * FROM student where class="+classs;				
					ResultSet rs = dataAccess.getData(query);
					
					ArrayList<String> sid = new ArrayList<String>();
					ArrayList<String> sname = new ArrayList<String>();
					while(rs.next()){
						
						sid.add(rs.getString("id"));
						sname.add(rs.getString("student_name"));
					}
					
					request.setAttribute("sid", sid);
					request.setAttribute("sname",sname);
					   
							
					RequestDispatcher dispatcher = request.getRequestDispatcher("teacher_add_marks.jsp");
					dispatcher.forward(request, response);

				}
				else
				{
					RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
					dispatcher.forward(request, response);
				}
				
	
		  			
			}catch(Exception ex){}
	}	

	
	

}