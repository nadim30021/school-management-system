import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;
import java.sql.SQLException;

public class AdminPostNoticeServlet extends HttpServlet{
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  try {
		  		HttpSession session=request.getSession(false);  
        		String n=(String)session.getAttribute("userid");
        		String p=(String)session.getAttribute("usertype");

        		if(p.equals("admin")){

        			request.setAttribute("id",n);
													
					RequestDispatcher dispatcher = request.getRequestDispatcher("admin_post_notice.jsp");
					dispatcher.forward(request, response);

				}
				else
				{
					RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
					dispatcher.forward(request, response);
				}
				
	
		  			
			}catch(Exception ex){}
	}




	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		 try {

		 		HttpSession session=request.getSession(false);  
        		String n=(String)session.getAttribute("userid");
        		String p=(String)session.getAttribute("usertype");

        		if(p.equals("admin")){
					request.setAttribute("id",n);

		 		String body = request.getParameter("post");
		 		Date date = Calendar.getInstance().getTime();
		 		 DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    String today = formatter.format(date);
    System.out.println("Today : " + today);
    			

		 		 System.out.println(today);

		 		MySqlDataAccess dataAccess = new MySqlDataAccess();
        		String query = "INSERT into notice (notice,notice_date) values('"+body+"','"+today+"')";				
				dataAccess.executeQuery(query);

													
				RequestDispatcher dispatcher = request.getRequestDispatcher("admin_post_notice.jsp");
				dispatcher.forward(request, response);
			}else
				{
					RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
					dispatcher.forward(request, response);
				}
        		
		  			
			}catch(Exception ex){
				
			}
		}

}
		