
import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;

import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;
import java.sql.SQLException;

public class GuestUserNoticeServlet extends HttpServlet{
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		 try {

		 		MySqlDataAccess dataAccess = new MySqlDataAccess();
        		String query = "SELECT * FROM notice ORDER BY notice_date desc";				
				ResultSet rs = dataAccess.getData(query);

				ArrayList<String> noticelist = new ArrayList<String>();
        		
				
        		while(rs.next())
        		{
					noticelist.add(rs.getString("notice"));
				
				}
				request.setAttribute("notice",noticelist);
													
				RequestDispatcher dispatcher = request.getRequestDispatcher("notice.jsp");
				dispatcher.forward(request, response);
        		
		  			
			}catch(Exception ex){
				
			}
		}

}
		