

import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;

import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;
import java.sql.SQLException;

public class StudentResultServlet extends HttpServlet{
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  try {

		  		HttpSession session=request.getSession(false);  
        		String n=(String)session.getAttribute("userid");
        		String p=(String)session.getAttribute("usertype");

        		if(p.equals("student")){

		  		
		  			MySqlDataAccess dataAccess = new MySqlDataAccess();
				

					String gradequery = "SELECT * FROM grade WHERE student_id="+n;
					ResultSet rs1 = dataAccess.getData(gradequery);

						
					while(rs1.next()){
						request.setAttribute(rs1.getString("Subject_Name"), rs1.getString("Marks"));
					}	

						
					RequestDispatcher dispatcher = request.getRequestDispatcher("student_home.jsp");
					dispatcher.forward(request, response);
				}
				else
				{
					RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
					dispatcher.forward(request, response);
				}

				
			}catch(Exception ex){
				
			}
	}	
}