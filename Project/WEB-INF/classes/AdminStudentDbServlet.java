import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;

import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;
import java.sql.SQLException;

public class AdminStudentDbServlet extends HttpServlet{
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  try {
		  		HttpSession session=request.getSession(false);  
        		String n=(String)session.getAttribute("userid");
        		String p=(String)session.getAttribute("usertype");

        		if(p.equals("admin")){
					request.setAttribute("id",n);
													
					RequestDispatcher dispatcher = request.getRequestDispatcher("admin_studentdb.jsp");
					dispatcher.forward(request, response);

				}

        		else
				{
					RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
					dispatcher.forward(request, response);
				}
				
	
		  			
			}catch(Exception ex){}
	}	

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  try {

		  		HttpSession session=request.getSession(false);  
        		String n=(String)session.getAttribute("userid");
        		String p=(String)session.getAttribute("usertype");

        		

        		if(p.equals("admin")){

        			String classs= request.getParameter("classs");


        			MySqlDataAccess dataAccess = new MySqlDataAccess();


        			String query = "SELECT * FROM student WHERE class="+classs;				
					ResultSet rs = dataAccess.getData(query);

					ArrayList<String> student_id = new ArrayList<String>();
					ArrayList<String> student_name = new ArrayList<String>();
					ArrayList<String> cl= new ArrayList<String>();
					ArrayList<String> password = new ArrayList<String>();
					ArrayList<String> address = new ArrayList<String>();
					ArrayList<String> DOB = new ArrayList<String>();
					ArrayList<String> fathers_name = new ArrayList<String>();
					ArrayList<String> mothers_name = new ArrayList<String>();

					while(rs.next()){
						System.out.println(query);
							student_id.add(rs.getString("id"));
							student_name.add(rs.getString("student_name"));
							cl.add(Integer.toString(rs.getInt("class")));
							password.add(rs.getString("password"));
							address.add(rs.getString("address"));
							DOB.add(rs.getString("DOB"));
							fathers_name.add(rs.getString("fathers_name"));
							mothers_name.add(rs.getString("mothers_name"));
					}

					request.setAttribute("student_id",student_id);
					request.setAttribute("student_name",student_name);
					request.setAttribute("cl",cl);
					request.setAttribute("password",password);
					request.setAttribute("address",address);
					request.setAttribute("DOB",DOB);
					request.setAttribute("fathers_name",fathers_name);
					request.setAttribute("mothers_name",mothers_name);
					
					RequestDispatcher dispatcher = request.getRequestDispatcher("admin_show_studentdb.jsp");
					dispatcher.forward(request, response);

        		
				}
				else
				{
					RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
					dispatcher.forward(request, response);
				}
	
			}catch(Exception ex){}
	}

}