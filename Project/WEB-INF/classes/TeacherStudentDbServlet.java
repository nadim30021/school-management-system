import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;

import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;
import java.sql.SQLException;

public class TeacherStudentDbServlet extends HttpServlet{
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  try {
		  		HttpSession session=request.getSession(false);  
        		String n=(String)session.getAttribute("userid");
        		String p=(String)session.getAttribute("usertype");

        		if(p.equals("teacher")){
					request.setAttribute("id",n);
					System.out.println("True");	
					RequestDispatcher dispatcher = request.getRequestDispatcher("teacher_home.jsp");
					dispatcher.forward(request, response);

				}

        		else
				{
					RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
					dispatcher.forward(request, response);
				}
				
	
		  			
			}catch(Exception ex){}
	}	

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  try {

		  		HttpSession session=request.getSession(false);  
        		String n=(String)session.getAttribute("userid");
        		String p=(String)session.getAttribute("usertype");

        		

        		if(p.equals("teacher")){

        			String classs= request.getParameter("classs");


        			MySqlDataAccess dataAccess = new MySqlDataAccess();


        			String query = "SELECT student.id,student_name,class,Subject_Name,Marks FROM student,grade WHERE student.id=grade.student_id and student.class="+classs;				
					System.out.println(query);
					ResultSet rs = dataAccess.getData(query);

					ArrayList<String> student_id = new ArrayList<String>();
					ArrayList<String> student_name = new ArrayList<String>();
					ArrayList<String> cl= new ArrayList<String>();					
					ArrayList<String> subject = new ArrayList<String>();
					ArrayList<String> marks = new ArrayList<String>();
					
					while(rs.next()){
						System.out.println(query);
							student_id.add(rs.getString("id"));
							student_name.add(rs.getString("student_name"));
							cl.add(Integer.toString(rs.getInt("class")));
							subject.add(rs.getString("Subject_Name"));
							marks.add(rs.getString("Marks"));
							
					}

					request.setAttribute("student_id",student_id);
					request.setAttribute("student_name",student_name);
					request.setAttribute("cl",cl);
					request.setAttribute("subject",subject);
					request.setAttribute("marks",marks);
									
					RequestDispatcher dispatcher = request.getRequestDispatcher("teacher_show_studentdb.jsp");
					dispatcher.forward(request, response);

        		
				}
				else
				{
					RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
					dispatcher.forward(request, response);
				}
	
			}catch(Exception ex){}
	}

}