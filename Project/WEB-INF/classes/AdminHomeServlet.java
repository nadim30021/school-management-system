import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;

import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;
import java.sql.SQLException;

public class AdminHomeServlet extends HttpServlet{
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  try {
		  		HttpSession session=request.getSession(false);  
        		String n=(String)session.getAttribute("userid");
        		String p=(String)session.getAttribute("usertype");

        		if(p.equals("admin")){
					request.setAttribute("id",n);
													
					RequestDispatcher dispatcher = request.getRequestDispatcher("admin_home_add.jsp");
					dispatcher.forward(request, response);

				}
				else
				{
					RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
					dispatcher.forward(request, response);
				}
				
	
		  			
			}catch(Exception ex){}
	}	

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  try {

		  		HttpSession session=request.getSession(false);  
        		String n=(String)session.getAttribute("userid");
        		String p=(String)session.getAttribute("usertype");

        		

        		if(p.equals("admin")){

        		String id = request.getParameter("id");
        		String name = request.getParameter("name");
		  		String fathers_name = request.getParameter("fname");
		  		String mothers_name= request.getParameter("mname");
		  		String dob= request.getParameter("dob");
		  		String address= request.getParameter("address");
		  		String pass= request.getParameter("password");
		  		String type= request.getParameter("type");
		  		
		  		if(type.equals("student"))
		  		{
		  			String classs= request.getParameter("class");

		  			MySqlDataAccess dataAccess = new MySqlDataAccess();
        			String query = "INSERT into student values('"+id+"','"+name+"',"+classs+",'"+pass+"','"+address+"','"+dob+"','"+fathers_name+"','"+mothers_name+"')";				
					dataAccess.executeQuery(query);
        		
					request.setAttribute("id", n);
													
					RequestDispatcher dispatcher = request.getRequestDispatcher("admin_home_add.jsp");
					dispatcher.forward(request, response);

		  		}

		  		else if(type.equals("teacher"))
		  		{
		  			String email= request.getParameter("email");
		  			MySqlDataAccess dataAccess = new MySqlDataAccess();
        			String query = "INSERT into teacher values('"+id+"','"+name+"','"+email+"','"+pass+"','"+address+"','"+dob+"','"+fathers_name+"','"+mothers_name+"')";				
					System.out.println(query);
					dataAccess.executeQuery(query);
        		
					request.setAttribute("id", n);
													
					RequestDispatcher dispatcher = request.getRequestDispatcher("admin_home_add.jsp");
					dispatcher.forward(request, response);

		  		}

				}
				else
				{
					RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
					dispatcher.forward(request, response);
				}
	
			}catch(Exception ex){}
	}

}