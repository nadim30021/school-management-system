-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 01, 2017 at 10:56 AM
-- Server version: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `xyzschool`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`) VALUES
('111', 'admin', 'admin@xyz.com', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `admin_email`
--

CREATE TABLE IF NOT EXISTS `admin_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_email` varchar(50) NOT NULL,
  `body` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `admin_email`
--

INSERT INTO `admin_email` (`id`, `sender_email`, `body`) VALUES
(1, 'lkdhxkjsl', 'sadas'),
(2, 'lkdhxkjsl', 'wfdsdg'),
(3, 'lkdhxkjsl', 'dfzf'),
(4, 'lkdhxkjsl', 'sdsa'),
(5, 'lkdhxkjsl', 'ewr'),
(6, 'lkdhxkjsl', 'abcd');

-- --------------------------------------------------------

--
-- Table structure for table `grade`
--

CREATE TABLE IF NOT EXISTS `grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` varchar(15) NOT NULL,
  `Subject_Name` varchar(15) NOT NULL,
  `Marks` int(11) NOT NULL,
  `Remarks` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `grade`
--

INSERT INTO `grade` (`id`, `student_id`, `Subject_Name`, `Marks`, `Remarks`) VALUES
(16, '111', 'Bangla', 80, 'Good'),
(17, 'gbhbc', 'Bangla', 70, 'Need to Improve');

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE IF NOT EXISTS `notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notice` varchar(500) NOT NULL,
  `notice_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`id`, `notice`, `notice_date`) VALUES
(1, '30/04/2017 school will be off.', '2017-04-29'),
(2, '05/05/2017 school will be off for some reasone.', '2017-05-03'),
(4, 'sdfdfds', '2017-04-30');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `id` varchar(10) NOT NULL,
  `student_name` varchar(50) NOT NULL,
  `class` int(11) NOT NULL,
  `password` varchar(30) NOT NULL,
  `address` varchar(100) NOT NULL,
  `DOB` varchar(10) NOT NULL,
  `fathers_name` varchar(30) NOT NULL,
  `mothers_name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `student_name`, `class`, `password`, `address`, `DOB`, `fathers_name`, `mothers_name`) VALUES
('111', 'abcd', 1, '111', 'sdd', 'gsd', 'dfssd', 'sdfs'),
('gbhbc', 'vcbcvb', 1, 'cvbcv', 'cvbcb', 'cvbcbv', 'cvbcv', 'cvbcv');

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE IF NOT EXISTS `teacher` (
  `id` varchar(10) NOT NULL,
  `teacher_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  `address` varchar(100) NOT NULL,
  `DOB` varchar(10) NOT NULL,
  `fathers_name` varchar(50) NOT NULL,
  `mothers_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`id`, `teacher_name`, `email`, `password`, `address`, `DOB`, `fathers_name`, `mothers_name`) VALUES
('111', 'djhsdfik', 'lkdhxkjsl', '111', 'xdjkj', 'jxjj', 'hxjhjjxc', 'jssjsjs'),
('112', 'ihdsjhjh', 'hjhhd@gmail.com', 'kjsdjkdjk', 'jxjzxjczkj', 'jkdzkfc', 'dkjkj', 'kjkxjk');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_email`
--

CREATE TABLE IF NOT EXISTS `teacher_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_email` varchar(30) NOT NULL,
  `student_id` varchar(15) NOT NULL,
  `email_subject` varchar(100) NOT NULL,
  `email_body` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `teacher_email`
--

INSERT INTO `teacher_email` (`id`, `sender_email`, `student_id`, `email_subject`, `email_body`) VALUES
(2, 'lkdhxkjsl', '111', 'test', 'Hello. This is just for test !!'),
(3, 'lkdhxkjsl', '111', 'test', 'Hello. This is ');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
